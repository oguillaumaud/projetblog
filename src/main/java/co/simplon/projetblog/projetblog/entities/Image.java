package co.simplon.projetblog.projetblog.entities;

public class Image {
    private Integer id;
    private String src;
    private String text;
    private Integer articleId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public Image(Integer id, String src,  String text, Integer articleId) {
        this.id = id;
        this.src = src;
        this.text = text;
        this.articleId = articleId;
    }

    public Image(String src, String text, Integer articleId) {
        this.src = src;
        this.text = text;
        this.articleId = articleId;
    }
    
    public Image() {
    }

}
