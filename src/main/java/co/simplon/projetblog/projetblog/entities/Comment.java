package co.simplon.projetblog.projetblog.entities;

import java.time.LocalDate;

public class Comment {
    private Integer id;
    private String paragraph;
    private LocalDate date;
    private Integer articleId;
    private Article article;
    private User owner;

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getParagraph() {
        return paragraph;
    }

    public void setParagraph(String paragraph) {
        this.paragraph = paragraph;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public Comment(Integer id, String paragraph, LocalDate date, Integer articleId) {
        this.id = id;
        this.paragraph = paragraph;
        this.date = date;
        this.articleId = articleId;
    }

    public Comment (String paragraph, LocalDate date, Integer articleId) {
        this.paragraph = paragraph;
        this.date = date;
        this.articleId = articleId;
    }
    
    public Comment() {
    }

}
