package co.simplon.projetblog.projetblog.entities;

public class Vue {
    private Integer id;
    private int like;
    private int dislike;
    private int vue;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public int getDislike() {
        return dislike;
    }

    public void setDislike(int dislike) {
        this.dislike = dislike;
    }

    public int getVue() {
        return vue;
    }

    public void setVue(int vue) {
        this.vue = vue;
    }

    public Vue(int like, int dislike, int vue) {
        this.like = like;
        this.dislike = dislike;
        this.vue = vue;
    }

    public Vue(Integer id, int like, int dislike, int vue) {
        this.id = id;
        this.like = like;
        this.dislike = dislike;
        this.vue = vue;
    }

    public Vue() {
    }
}
