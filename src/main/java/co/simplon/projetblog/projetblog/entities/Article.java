package co.simplon.projetblog.projetblog.entities;

import java.time.LocalDate;
import java.util.List;

public class Article {

    private Integer id;
    private String title;
    private String paragraph;
    private Category category;
    private Integer idCategory;
    private LocalDate date;
    List<Image> images;
    private int vue;
    private User owner;

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public int getVue() {
        return vue;
    }

    public void setVue(int vue) {
        this.vue = vue;
    }

    public Integer getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Integer idCategory) {
        this.idCategory = idCategory;
    }

    public Article(String title, String paragraph, Integer idCategory, LocalDate date, int vue) {
        this.title = title;
        
        this.paragraph = paragraph;
        this.idCategory = idCategory;
        this.date = date;
        this.vue = vue;
    }

    public Article() {
    }

    public Article(Integer id, String title, String paragraph, Integer idCategory, LocalDate date, int vue) {
        this.id = id;
        this.title = title;
        this.paragraph = paragraph;
        this.idCategory = idCategory;
        this.date = date;
        this.vue = vue;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParagraph() {
        return paragraph;
    }

    public void setParagraph(String paragraph) {
        this.paragraph = paragraph;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }



}
