package co.simplon.projetblog.projetblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetBlogApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetBlogApplication.class, args);
	}

}
