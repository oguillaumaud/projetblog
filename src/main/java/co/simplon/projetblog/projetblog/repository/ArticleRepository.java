package co.simplon.projetblog.projetblog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.projetblog.projetblog.entities.Article;
import co.simplon.projetblog.projetblog.entities.Category;
import co.simplon.projetblog.projetblog.entities.User;

@Repository
public class ArticleRepository {

    @Autowired
    private DataSource datasource;

    @Autowired
    private ImageRepository imgRepo;

    @Autowired
    private VueRepository vueRepo;

    
    public List<Article> findByUser(int idUser) {
    List<Article> list = new ArrayList<>();
    try (Connection connection = datasource.getConnection()) {
    PreparedStatement stmt = connection.prepareStatement("SELECT*FROM article LEFT JOIN user ON article.id_user = user.id WHERE id_user = ?");
    stmt.setInt(1, idUser);
    ResultSet result = stmt.executeQuery();
    while (result.next()) {
    list.add( new Article(result.getInt("id"), 
            result.getString("title"),
            result.getString("paragraph"), result.getInt("id_category"),
            result.getDate("date").toLocalDate(),
            result.getInt("vue")));
    }
    } catch (Exception e) {
    e.printStackTrace();
    throw new RuntimeException("Error in repository", e);
    }
    return list;
    }

    public List<Article> findAll() {
        List<Article> list = new ArrayList<>();
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT article.*, category.name category_name, user.* FROM article LEFT JOIN category ON article.id_category = category.id LEFT JOIN user ON article.id_user = user.id ORDER BY article.id DESC");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Article article = new Article(result.getInt("id"), 
                result.getString("title"),
                        result.getString("paragraph"), result.getInt("id_category"),
                        result.getDate("date").toLocalDate(),
                        result.getInt("vue"));

                article.setCategory(new Category(
                    result.getInt("id_category"), 
                    result.getString("category_name")));

                article.setOwner(new User(
                    result.getInt("id_user"),
                    result.getString("username"), 
                    result.getString("email"), 
                    null, 
                    result.getString("role")));

                article.setImages(imgRepo.findAllOfArticle(result.getInt("id")));
                list.add(article);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }

    public List<Article> findAllLast() {
        List<Article> list = new ArrayList<>();
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT article.*, category.name category_name, user.* FROM article LEFT JOIN category ON article.id_category = category.id LEFT JOIN user ON article.id_user = user.id ORDER BY article.id DESC LIMIT 3");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Article article = new Article(result.getInt("id"), 
                result.getString("title"),
                        result.getString("paragraph"), result.getInt("id_category"),
                        result.getDate("date").toLocalDate(),
                        result.getInt("vue"));

                article.setCategory(new Category(
                    result.getInt("id_category"), 
                    result.getString("category_name")));

                article.setOwner(new User(
                    result.getInt("id_user"),
                    result.getString("username"), 
                    result.getString("email"), 
                    null, 
                    result.getString("role")));

                article.setImages(imgRepo.findAllOfArticle(result.getInt("id")));
                list.add(article);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }

    public Article findById(int id) {
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT*FROM article LEFT JOIN user ON article.id_user = user.id WHERE article.id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Article article = new Article(
                        result.getInt("id"),
                        result.getString("title"),
                        result.getString("paragraph"), result.getInt("id_category"),
                        result.getDate("date").toLocalDate(),
                        result.getInt("vue"));
                article.setImages(imgRepo.findAllOfArticle(result.getInt("id")));
                vueRepo.findById(id);

                article.setOwner(new User(
                result.getInt("id_user"),
                result.getString("username"), 
                result.getString("email"), 
                null, 
                result.getString("role")));

                return article;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return null;
    }

    /*
     * public List<Article> findByCategory(String category) {
     * List<Article> list = new ArrayList<>();
     * try (Connection connection = datasource.getConnection()) {
     * PreparedStatement stmt =
     * connection.prepareStatement("SELECT*FROM article WHERE category = ?");
     * stmt.setString(1, category);
     * ResultSet result = stmt.executeQuery();
     * while (result.next()) {
     * list.add(new Article(result.getInt("id"), result.getString("title"),
     * result.getString("author"),
     * result.getString("paragraph"), result.getString("category"),
     * result.getDate("date").toLocalDate()));
     * }
     * } catch (Exception e) {
     * e.printStackTrace();
     * throw new RuntimeException("Error in repository", e);
     * }
     * return list;
     * }
     */

    public Boolean percist(Article article) {
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection
                    .prepareStatement("INSERT INTO article (title, paragraph, id_category, id_user) VALUES (?,?,?,?)",
                            Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, article.getTitle());
            stmt.setString(2, article.getParagraph());
            stmt.setInt(3, article.getCategory().getId());
            stmt.setInt(4, article.getOwner().getId());
            int result = stmt.executeUpdate();
            if (result == 1) {
                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                article.setId(rs.getInt(1));
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return false;
    }

    public Boolean delete(int id) {
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM article WHERE id = ?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Boolean update(Article article) {
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection
                    .prepareStatement(
                            "UPDATE article SET title = ?, paragraph = ?, id_category = ? WHERE id = ?");
            stmt.setString(1, article.getTitle());
            stmt.setString(2, article.getParagraph());
            stmt.setInt(3, article.getIdCategory());
            stmt.setInt(4, article.getId());
            int result = stmt.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return false;
    }
}
