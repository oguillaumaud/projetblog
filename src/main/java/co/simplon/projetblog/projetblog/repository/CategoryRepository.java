package co.simplon.projetblog.projetblog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.projetblog.projetblog.entities.Category;

@Repository
public class CategoryRepository {
    
    @Autowired
    private DataSource datasource;

    public List<Category> findAll() {
        List<Category> list = new ArrayList<>();
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT*FROM category");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(new Category(result.getInt("id"), result.getString("name")));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }

    public Category findById(int id) {
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT*FROM category WHERE id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Category(result.getInt("id"), result.getString("name"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return null;
    }

   /* public List<Article> findAllArticles(int id) {
        List<Article> list = new ArrayList<>();
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT*FROM article WHERE category = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(new Article(result.getInt("id"), result.getString("title"), result.getString("author"),
                        result.getString("paragraph"), result.getInt("category"),
                        result.getDate("date").toLocalDate()));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    } */
    
    public Boolean percist(Category category) {
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection
                    .prepareStatement("INSERT INTO category (name) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, category.getName());
            int result = stmt.executeUpdate();
            if (result == 1) {
                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                category.setId(rs.getInt(1));
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return false;
    }

    public Boolean delete(int id) {
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM category WHERE id = ?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Boolean update(Category category) {
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection
                    .prepareStatement("UPDATE category SET name = ? WHERE id = ?");
            stmt.setString(1, category.getName());
            stmt.setInt(2, category.getId());
            int result = stmt.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return false;
    }
}
