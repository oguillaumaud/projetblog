package co.simplon.projetblog.projetblog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.projetblog.projetblog.entities.Comment;
import co.simplon.projetblog.projetblog.entities.User;

@Repository
public class CommentRepository {

    @Autowired
    private DataSource datasource;

    public List<Comment> findAllOfArticle(int id) {
        List<Comment> list = new ArrayList<>();
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT*FROM comment LEFT JOIN user ON comment.id_user = user.id WHERE article_id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Comment comment = new Comment(result.getInt("id"),
                        result.getString("paragraph"),
                        result.getDate("date").toLocalDate(), result.getInt("article_id"));

                comment.setOwner(new User(
                        result.getInt("id_user"),
                        result.getString("username"),
                        result.getString("email"),
                        null,
                        result.getString("role")));

                        list.add(comment);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }

    public Boolean percist(Comment comment) {
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection
                    .prepareStatement("INSERT INTO comment (paragraph, article_id, id_user) VALUES (?,?,?)",
                            Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, comment.getParagraph());
            stmt.setInt(2, comment.getArticleId());
            stmt.setInt(3, comment.getOwner().getId());
            int result = stmt.executeUpdate();
            if (result == 1) {
                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                comment.setId(rs.getInt(1));
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return false;
    }

    public Boolean delete(int id) {
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM comment WHERE id = ?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Boolean update(Comment comment) {
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection
                    .prepareStatement("UPDATE comment SET paragraph = ?, article_id = ? WHERE id = ?");
            stmt.setString(1, comment.getParagraph());
            stmt.setInt(2, comment.getArticleId());
            stmt.setInt(3, comment.getId());
            int result = stmt.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return false;
    }

}
