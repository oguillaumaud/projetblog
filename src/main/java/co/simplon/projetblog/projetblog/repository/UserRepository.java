package co.simplon.projetblog.projetblog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.projetblog.projetblog.entities.User;

@Repository
public class UserRepository {
    @Autowired
    private DataSource dataSource;

    public Optional<User> findByEmail(String email) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE email=?");
            stmt.setString(1, email);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                User user = new User(
                        result.getInt("id"),
                        result.getString("username"),
                        result.getString("email"),
                        result.getString("password"),
                        result.getString("role"));
                return Optional.of(user);
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return Optional.empty();
    }

    public Optional<User> findById(int id) {

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if(result.next()) {
                User user = new User(
                        result.getInt("id"),
                        result.getString("username"),
                        result.getString("email"),
                        result.getString("password"),
                        result.getString("role"));
                return Optional.of(user);
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return Optional.empty();
    }

    public boolean persist(User user) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO user (username, email, password, role) VALUES (?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, user.getUserName());
            stmt.setString(2, user.getEmail());
            stmt.setString(3, user.getPassword());
            stmt.setString(4, user.getRole());
            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                if (result.next()) {
                    user.setId(result.getInt(1));
                    return true;
                }
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return false;
    }

}
