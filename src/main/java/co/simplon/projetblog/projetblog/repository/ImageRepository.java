package co.simplon.projetblog.projetblog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.projetblog.projetblog.entities.Image;

@Repository
public class ImageRepository {

    @Autowired
    private DataSource datasource;

    public List<Image> findAllOfArticle(int id) {
        List<Image> list = new ArrayList<>();
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT*FROM image WHERE article_id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(new Image(result.getInt("id"), result.getString("src"), result.getString("text"), result.getInt( "article_id")));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }

    public Boolean percist(Image image) {
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection
                    .prepareStatement("INSERT INTO image ( src, text, article_id) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, image.getSrc());
            stmt.setString(2, image.getText());
            stmt.setInt(3, image.getArticleId());
            int result = stmt.executeUpdate();
            if (result == 1) {
                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                image.setId(rs.getInt(1));
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return false;
    }

    public Boolean delete(int id) {
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM image WHERE id = ?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    public Boolean update(Image image) {
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection
                    .prepareStatement("UPDATE image SET src = ?, text = ? WHERE id = ?");
            stmt.setString(1, image.getSrc());
            stmt.setString(2, image.getText());
            stmt.setInt(3, image.getId());
            int result = stmt.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return false;
    }
}
