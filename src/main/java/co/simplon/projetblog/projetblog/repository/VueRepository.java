package co.simplon.projetblog.projetblog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.projetblog.projetblog.entities.Vue;

@Repository
public class VueRepository {

    @Autowired
    private DataSource datasource;

    public List<Vue> findAll() {
        List<Vue> list = new ArrayList<>();
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT*FROM vue");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(new Vue(result.getInt("id"), result.getInt("vue"), result.getInt("like_article"), result.getInt("dislike")));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }

    public Vue findById(int id) {
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT*FROM vue WHERE id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                return new Vue(result.getInt("id"), result.getInt("vue"), result.getInt("like_article"), result.getInt("dislike"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return null;
    }

    public Boolean percist(Vue vue) {
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection
                    .prepareStatement("INSERT INTO vue (vue, like_article, dislike) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, vue.getVue());
            stmt.setInt(2, vue.getLike());
            stmt.setInt(3, vue.getDislike());
            int result = stmt.executeUpdate();
            if (result == 1) {
                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                vue.setId(rs.getInt(1));
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return false;
    }

    public Boolean delete(int id) {
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM vue WHERE id = ?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Boolean update(Vue vue) {
        try (Connection connection = datasource.getConnection()) {
            PreparedStatement stmt = connection
                    .prepareStatement("UPDATE vue SET vue = ?, like_article = ?, dislike = ? WHERE id = ?");
            stmt.setInt(1, vue.getVue());
            stmt.setInt(2, vue.getLike());
            stmt.setInt(3, vue.getDislike());
            stmt.setInt(4, vue.getId());
            int result = stmt.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return false;
    }
}
