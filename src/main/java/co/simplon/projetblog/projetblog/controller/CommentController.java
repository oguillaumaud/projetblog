package co.simplon.projetblog.projetblog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.projetblog.projetblog.entities.Comment;
import co.simplon.projetblog.projetblog.entities.User;
import co.simplon.projetblog.projetblog.repository.CommentRepository;

@RestController
@RequestMapping("/api/comment")
public class CommentController {

    @Autowired
    private CommentRepository repo;

    @GetMapping("/{id}")
    public List<Comment> getAllOfArticle(@PathVariable int id) {
        return repo.findAllOfArticle(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Comment postPercist(@RequestBody Comment comment, @AuthenticationPrincipal User user) {
        comment.setOwner(user);
        repo.percist(comment);
        return comment;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id, @AuthenticationPrincipal User user) {
        repo.delete(id);
    }

    @PutMapping("{id}")
    public Comment putMethodName(@PathVariable int id, @RequestBody Comment comment,
            @AuthenticationPrincipal User user) {
        repo.update(comment);
        return comment;
    }
}
