package co.simplon.projetblog.projetblog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.projetblog.projetblog.entities.Image;
import co.simplon.projetblog.projetblog.repository.ImageRepository;

@RestController
@RequestMapping("/api/image")
public class ImageController {

    @Autowired
    private ImageRepository repo;

    @GetMapping("/{id}")
    public List<Image> getAllOfArticle(@PathVariable int id) {
        return repo.findAllOfArticle(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Image postPercist(@RequestBody Image image) {
        repo.percist(image);
        return image;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        repo.delete(id);
    }

    @PutMapping("{id}")
    public Image putMethodName(@PathVariable int id, @RequestBody Image image) {
        repo.update(image);
        return image;
    }
}
