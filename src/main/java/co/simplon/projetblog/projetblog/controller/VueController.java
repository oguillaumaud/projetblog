package co.simplon.projetblog.projetblog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.projetblog.projetblog.entities.Vue;
import co.simplon.projetblog.projetblog.repository.VueRepository;

@RestController
@RequestMapping("/api/vue")
public class VueController {

    @Autowired
    private VueRepository repo;

    @GetMapping
    public List<Vue> getAll() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Vue getAllOfArticle(@PathVariable int id) {
        return repo.findById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Vue postPercist(@RequestBody Vue vue) {
        repo.percist(vue);
        return vue;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        repo.delete(id);
    }

    @PutMapping("{id}")
    public Vue putMethodName(@PathVariable int id, @RequestBody Vue vue) {
        repo.update(vue);
        return vue;
    }
}
