package co.simplon.projetblog.projetblog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.projetblog.projetblog.entities.Article;
import co.simplon.projetblog.projetblog.entities.Image;
import co.simplon.projetblog.projetblog.entities.User;
import co.simplon.projetblog.projetblog.repository.ArticleRepository;
import co.simplon.projetblog.projetblog.repository.ImageRepository;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;




@RestController
@RequestMapping("/api/article")
public class ArticleController {

    @Autowired
    private ArticleRepository repo;

    @Autowired
    private ImageRepository repoImage;

    @GetMapping
    public List<Article> getAll() {
        return repo.findAll();
    }

    @GetMapping("/latest")
    public List<Article> getlast() {
        return repo.findAllLast();
    }
    
    @GetMapping("/{id}")
    public Article getByID(@PathVariable int id) {
        Article article = repo.findById(id);
        if (article == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        article.setVue(article.getVue()+1);
        repo.update(article);
        return article;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Article postPercist(@RequestBody Article article, @AuthenticationPrincipal User user) {
        article.setOwner(user);
        repo.percist(article);
        for (Image image : article.getImages()) {
            image.setArticleId(article.getId());
            repoImage.percist(image);
        }
        return article;
    }
    
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id, @AuthenticationPrincipal User user) {
        getByID(id);
        repo.delete(id);
    }

    @PutMapping("{id}")
    public Article putMethodName(@PathVariable int id, @RequestBody Article article, @AuthenticationPrincipal User user) {
        repo.update(article);
        return article;
    }
}
