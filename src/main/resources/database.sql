-- Active: 1709546324780@@127.0.0.1@3306@projet_blog
DROP TABLE IF EXISTS comment;
DROP TABLE IF EXISTS vue;
DROP TABLE IF EXISTS image;
DROP TABLE IF EXISTS article;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS user;

CREATE TABLE user (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(64) NOT NULL UNIQUE,
    email VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    role VARCHAR(64)
);

CREATE TABLE category(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL
);

CREATE TABLE article(
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(128) NOT NULL,
    id_category INT NOT NULL,
    Foreign Key (id_category) REFERENCES category(id),
    paragraph VARCHAR(1000),
    vue INT DEFAULT 0,
    date DATE DEFAULT CURRENT_TIMESTAMP,
    id_user INT,
    Foreign Key (id_user) REFERENCES user(id)
);

CREATE TABLE image(
    id INT PRIMARY KEY AUTO_INCREMENT,
    src VARCHAR(1000) NOT NULL,
    text VARCHAR(500),
    article_id INT,
    Foreign Key (article_id) REFERENCES article(id) ON DELETE SET NULL
);

CREATE TABLE vue(
    id INT PRIMARY KEY AUTO_INCREMENT,
    vue INT,
    like_article INT,
    dislike INT
);

CREATE TABLE comment(
    id INT PRIMARY KEY AUTO_INCREMENT,
    paragraph VARCHAR(1000) NOT NULL,
    date DATE DEFAULT CURRENT_TIMESTAMP,
    article_id INT,
    Foreign Key (article_id) REFERENCES article(id) ON DELETE SET NULL,
    id_user INT,
    Foreign Key (id_user) REFERENCES user(id)
);

INSERT INTO user (username, email, password, role) VALUES
("admin", "admin@test.com", "$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq", "ROLE_ADMIN"),
("Pom","pom@test.com", "$2a$12$Iecy.0VI3Q2A8LeFgd32cOqB3WbY6otlQ.7UQMV9O2cHDNO9dMJBy", "ROLE_USER"),
("Robin des Bois","rdb@test.com", "$2a$12$Iecy.0VI3Q2A8LeFgd32cOqB3WbY6otlQ.7UQMV9O2cHDNO9dMJBy", "ROLE_USER"),
("Yubaba","yu@test.com", "$2a$12$Iecy.0VI3Q2A8LeFgd32cOqB3WbY6otlQ.7UQMV9O2cHDNO9dMJBy", "ROLE_USER"),
("Anonymous","anonym@test.com", "$2a$12$Iecy.0VI3Q2A8LeFgd32cOqB3WbY6otlQ.7UQMV9O2cHDNO9dMJBy", "ROLE_USER"),
("Alex","alex@test.com", "$2a$12$Iecy.0VI3Q2A8LeFgd32cOqB3WbY6otlQ.7UQMV9O2cHDNO9dMJBy", "ROLE_USER"),
("Hood","hood@test.com", "$2a$12$Iecy.0VI3Q2A8LeFgd32cOqB3WbY6otlQ.7UQMV9O2cHDNO9dMJBy", "ROLE_USER");

INSERT INTO category (name) VALUES
("animal"),
("paysage"),
("flore"),
("ciel");

INSERT INTO article(title, id_category, paragraph, `date`, vue, id_user) VALUES 
("Marmotte", 1,"Voici une photo d'une marmotte que nous avons croisée lors d'une randonnée dans les Alpes. Nous avons pu prendre le temps de l'observer vaqué à ses occupations.", "2023-07-15", 500,1),
("Forêt", 2, "Un rayon de soleil perse a travers les arbres de cette forêt, magique.", "2023-09-01", 254,1),
("Forêt des Brume", 2, "L'automne est arriver l'a brume s'installe dans la forêt près de chez nous. Je vous partage cet instant magique.", "2023-10-10", 258,2),
("Une Bulle de glace", 2, "L'hiver est là, en jouant avec des bulles ce matin, nous avons pu les voir peu, a, peut se transformer en glace.", "2024-01-10", 178, 6),
("Lycoris",3,"Magnifiques floraisons de Lys araignée.", "2024-04-01", 80, 4);

INSERT INTO image (src, text, article_id) VALUES 
("https://plus.unsplash.com/premium_photo-1675716926653-8f4529595012?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D","Une marmotte", 2),
("https://images.unsplash.com/photo-1448375240586-882707db888b?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D","Photo d'une foret avec un réyon de soleil", 1),
("https://images.unsplash.com/photo-1596237563267-84ffd99c80e1?q=80&w=1974&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D","Photo d'une forets envahie par la brume", 3),
("https://images.unsplash.com/photo-1484278786775-527ac0d0b608?q=80&w=1961&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D", "Une bulle prise dans la glace, se matin d'hiver", 4),
("https://cdn.discordapp.com/attachments/1107311879522619394/1228281129405120583/photo-1661134185883-cfdcbd8941de.png?ex=6634b371&is=66223e71&hm=412972708fe1341dbf8343bd2f847bf01e9a43ea96484efff283f71c736f0ff9&", "Lycoris en fleur", 5);

INSERT INTO vue (vue, like_article, dislike) VALUES
(155, 50,20),
(205, 100, 50),
(500, 200, 80);

INSERT INTO comment (paragraph, article_id, id_user) VALUES
("Trop mignon !!", 1, 6),
("On dirais qu'elle vous regarde", 1, 1),
("Magnifique !", 2, 3),
("J'installerais bien ma maison de sorcière dans cette forêt.", 3, 4),
("L'hiver vient", 3, 5),
("OoO !!", 3, 7);


