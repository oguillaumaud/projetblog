package co.simplon.projetblog.projetblog.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class CategoryTest {

    @Autowired
    MockMvc mvc;
    
        @Test
    void testGetAll() throws Exception {
        mvc.perform(get("/api/category"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[*]['name']").exists());
    }

    @Test
    void testGetByIdSucces() throws Exception{
        mvc.perform(get("/api/category/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$['name']").isString());
    }

    @Test
    void testGetByIdNotFound() throws Exception{
        mvc.perform(get("/api/category/9000"))
        .andExpect(status().isNotFound());
    }

    @Test
    void testPostCategory() throws Exception {
        mvc.perform(
            post("/api/category")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "name": "test"
                }
            """)
            ).andExpect(status().isCreated())
            .andExpect(jsonPath("$['id']").isNumber())
            .andExpect(jsonPath("$['name']").isString());
    }

    @Test
    void testPostCategoryNotSucces() throws Exception {
        mvc.perform(post("/api/category")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "name": ""
                }
            """)
            ).andExpect(status().isBadRequest());
    }

    @Test
    void deleteTestCategory() throws Exception{
        mvc.perform(delete("/api/category/1"))
        .andExpect(status().isNoContent());
    }

    @Test
    void testPutCategory() throws Exception {
        mvc.perform(put("/api/category/1")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "id": 1,
                    "name": "update"
                }
            """)
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['id']").value(1))
            .andExpect(jsonPath("$['name']").value("update"));
    }
    
    @Test
    void testPutCategoryFail() throws Exception {
        mvc.perform(put("/api/category/1")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                { 
                    "id": 1
                }
            """)
            ).andExpect(status().isBadRequest());
    }
}
