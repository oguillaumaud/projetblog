package co.simplon.projetblog.projetblog.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class ImageTest {

    @Autowired
    MockMvc mvc;

    @Test
    void testGetAll() throws Exception {
        mvc.perform(get("/api/image/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[*]['src']").exists())
        .andExpect(jsonPath("$[*]['text']").exists())
        .andExpect(jsonPath("$[*]['articleId']").exists());
    }

    @Test
    void testPostImage() throws Exception {
        mvc.perform(
            post("/api/image")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "src": "lul",
                    "text": "Photo d'une foret avec un réyon de soleil",
                    "articleId": 2
                }
            """)
            ).andExpect(status().isCreated())
            .andExpect(jsonPath("$['id']").isNumber())
            .andExpect(jsonPath("$['src']").isString())
            .andExpect(jsonPath("$['text']").isString())
            .andExpect(jsonPath("$['articleId']").isNumber());
    }

    @Test
    void testPostImageNotSucces() throws Exception {
        mvc.perform(post("/api/image")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "src": "lul",
                    "text": "Photo d'une foret avec un réyon de soleil",
                }
            """)
            ).andExpect(status().isBadRequest());
    }

    @Test
    void deleteTestImage() throws Exception{
        mvc.perform(delete("/api/image/1"))
        .andExpect(status().isNoContent());
    }

    @Test
    void testPutImage() throws Exception {
        mvc.perform(put("/api/image/1")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "id": 1,
                    "src": "test",
                    "text": "test",
                    "articleId": 2
                }
            """)
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['id']").value(1))
            .andExpect(jsonPath("$['src']").value("test"))
            .andExpect(jsonPath("$['text']").value("test"))
            .andExpect(jsonPath("$['articleId']").value(2));
    }
    
    @Test
    void testPutImageFail() throws Exception {
        mvc.perform(put("/api/image/1")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "src": "test",
                    "text": "test",
                }
            """)
            ).andExpect(status().isBadRequest());
    }
}
