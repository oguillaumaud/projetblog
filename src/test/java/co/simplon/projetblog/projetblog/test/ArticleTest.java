package co.simplon.projetblog.projetblog.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class ArticleTest {

    @Autowired
    MockMvc mvc;

    @Test
    void testGetAll() throws Exception {
        mvc.perform(get("/api/article"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[*]['title']").exists())
        .andExpect(jsonPath("$[*]['author']").exists())
        .andExpect(jsonPath("$[*]['paragraph']").exists())
        .andExpect(jsonPath("$[*]['category']").exists())
        .andExpect(jsonPath("$[*]['date']").exists());
    }

    @Test
    void testGetByIdSucces() throws Exception{
        mvc.perform(get("/api/article/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$['title']").isString())
        .andExpect(jsonPath("$['author']").isString())
        .andExpect(jsonPath("$['paragraph']").isString())
        .andExpect(jsonPath("$['category']").isNumber())
        .andExpect(jsonPath("$['date']").exists());
    }

    @Test
    void testGetByIdNotFound() throws Exception{
        mvc.perform(get("/api/article/9000"))
        .andExpect(status().isNotFound());
    }

    @Test
    void testPostArticle() throws Exception {
        mvc.perform(
            post("/api/article")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "title": "un titre",
                    "author": "un auteur",
                    "paragraph": "un petit paragraphe",
                    "category": 1
                }
            """)
            ).andExpect(status().isCreated())
            .andExpect(jsonPath("$['id']").isNumber())
            .andExpect(jsonPath("$['title']").isString())
            .andExpect(jsonPath("$['author']").isString())
            .andExpect(jsonPath("$['paragraph']").isString())
            .andExpect(jsonPath("$['category']").isNumber());
    }

    @Test
    void testPostArticleNotSucces() throws Exception {
        mvc.perform(post("/api/article")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "author": "un auteur",
                    "paragraph": "un petit paragraphe",
                }
            """)
            ).andExpect(status().isBadRequest());
    }

    @Test
    void deleteTestArticle() throws Exception{
        mvc.perform(delete("/api/article/1"))
        .andExpect(status().isNoContent());
    }

    @Test
    void testPutArticle() throws Exception {
        mvc.perform(put("/api/article/1")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "id": 1,
                    "title": "un titre update",
                    "author": "un auteur update",
                    "paragraph": "un petit paragraphe update",
                    "category": 1
                }
            """)
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['id']").value(1))
            .andExpect(jsonPath("$['title']").value("un titre update"))
            .andExpect(jsonPath("$['author']").value("un auteur update"))
            .andExpect(jsonPath("$['paragraph']").value("un petit paragraphe update"))
            .andExpect(jsonPath("$['category']").value(1));
    }
    
    @Test
    void testPutArticleFail() throws Exception {
        mvc.perform(put("/api/article/2")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "author": "un auteur update",
                    "paragraph": "un petit paragraphe update",
                    "category": 1,
                }
            """)
            ).andExpect(status().isBadRequest());
    }
}
