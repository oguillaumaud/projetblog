package co.simplon.projetblog.projetblog.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class VueTest {

    @Autowired
    MockMvc mvc;

    @Test
    void testGetAll() throws Exception {
        mvc.perform(get("/api/vue"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[*]['like']").exists())
        .andExpect(jsonPath("$[*]['dislike']").exists())
        .andExpect(jsonPath("$[*]['vue']").exists());
    }

    @Test
    void testGetByIdSucces() throws Exception{
        mvc.perform(get("/api/vue/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$['vue']").isNumber())
        .andExpect(jsonPath("$['like']").isNumber())
        .andExpect(jsonPath("$['dislike']").isNumber());
    }

    @Test
    void testGetByIdNotFound() throws Exception{
        mvc.perform(get("/api/vue/9000"))
        .andExpect(status().isNotFound());
    }

    @Test
    void testPostVue() throws Exception {
        mvc.perform(
            post("/api/vue")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "like": 205,
                    "dislike": 100,
                    "vue": 50
                }
            """)
            ).andExpect(status().isCreated())
            .andExpect(jsonPath("$['vue']").isNumber())
            .andExpect(jsonPath("$['like']").isNumber())
            .andExpect(jsonPath("$['dislike']").isNumber());
    }

    @Test
    void testPostVueNotSucces() throws Exception {
        mvc.perform(post("/api/vue")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "like": 205,
                    "dislike": 100,
                }
            """)
            ).andExpect(status().isBadRequest());
    }

    @Test
    void deleteTestVue() throws Exception{
        mvc.perform(delete("/api/vue/1"))
        .andExpect(status().isNoContent());
    }

    @Test
    void testPutVue() throws Exception {
        mvc.perform(put("/api/vue/1")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "id": 1,
                    "like": 0,
                    "dislike": 0,
                    "vue": 0,
                    "idArticle": 1
                }
            """)
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['vue']").isNumber())
            .andExpect(jsonPath("$['like']").isNumber())
            .andExpect(jsonPath("$['dislike']").isNumber())
            .andExpect(jsonPath("$['idArticle']").isNumber());
    }
    
    @Test
    void testPutVueFail() throws Exception {
        mvc.perform(put("/api/vue/2")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "id": 2,
                    "like": 205,
                }
            """)
            ).andExpect(status().isBadRequest());
    }
}
