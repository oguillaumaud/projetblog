package co.simplon.projetblog.projetblog.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class CommentTest {

    @Autowired
    MockMvc mvc;

    @Test
    void testGetAll() throws Exception {
        mvc.perform(get("/api/comment/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[*]['author']").exists())
        .andExpect(jsonPath("$[*]['paragraph']").exists())
        .andExpect(jsonPath("$[*]['date']").exists())
        .andExpect(jsonPath("$[*]['articleId']").exists());
    }

    @Test
    void testPostComment() throws Exception {
        mvc.perform(
            post("/api/comment")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "author": "Robin des Bois",
                    "paragraph": "Trop mignon !!",
                    "articleId": 1
                }
            """)
            ).andExpect(status().isCreated())
            .andExpect(jsonPath("$['id']").isNumber())
            .andExpect(jsonPath("$['author']").isString())
            .andExpect(jsonPath("$['paragraph']").isString())
            .andExpect(jsonPath("$['articleId']").isNumber());
    }

    @Test
    void testPostCommentNotSucces() throws Exception {
        mvc.perform(post("/api/comment")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "author": "un auteur",
                }
            """)
            ).andExpect(status().isBadRequest());
    }

    @Test
    void deleteTestComment() throws Exception{
        mvc.perform(delete("/api/comment/1"))
        .andExpect(status().isNoContent());
    }

    @Test
    void testPutComment() throws Exception {
        mvc.perform(put("/api/comment/1")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "id": 1,
                    "author": "test",
                    "paragraph": "test",
                    "articleId": 1
                }
            """)
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['id']").value(1))
            .andExpect(jsonPath("$['author']").value("test"))
            .andExpect(jsonPath("$['paragraph']").value("test"))
            .andExpect(jsonPath("$['articleId']").value(1));
    }
    
    @Test
    void testPutCommentFail() throws Exception {
        mvc.perform(put("/api/comment/1")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "author": "un auteur update",
                    "paragraph": "un petit paragraphe update",
                }
            """)
            ).andExpect(status().isBadRequest());
    }

}
